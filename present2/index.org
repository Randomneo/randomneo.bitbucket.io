#+REVEAL_HIGHLIGHT_CSS: ./css/dracula.css
#+REVEAL_ROOT: ../
#+REVEAL_THEME: league
#+REVEAL_PLUGINS: (highlight)
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: reveal_title_slide:nil

* lesson 2
  - IDE's and compilers
  - 2d arrays
* IDE
#+ATTR_REVEAL: :frag (appear)
  1. Visual Studio
  2. Code::Blocks
  3. Qt Creator
  4. CLion
  5. DevC++
* why do we use IDE
#+ATTR_REVEAL: :frag (appear)
  - Speedup
  - UX (easy to use)
* Arrays
#+BEGIN_SRC C++
int t[100][100];
#+END_SRC
#+BEGIN_SRC C++
  for (int i = 0; i < 100; i++)
      for (int j = 0; j < 100; j++)
	  cin >> t[i][j];
#+END_SRC
* How its works
  How array stored in memory?
  #+ATTR_REVEAL: :frag (appear)
| 0x12 | 0x13 | 0x14 | 0x15 | 0x16 |
|------+------+------+------+------|
|    1 |    2 |    3 |    4 |    5 |
